package net.minecraftforge.fml.common.eventhandler;

import cpw.mods.fml.common.eventhandler.EventPriority;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


    @Retention(value = RUNTIME)
    @Target(value = METHOD)
    public @interface SubscribeEvent
    {
        public EventPriority priority() default EventPriority.NORMAL;
        public boolean receiveCanceled() default false;
    }

