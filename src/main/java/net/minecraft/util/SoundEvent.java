package net.minecraft.util;
import cpw.mods.fml.common.registry.GameData;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SoundCategory;
import net.minecraft.client.audio.SoundEventAccessorComposite;


public class SoundEvent {
    public static final net.minecraft.util.registry.RegistryNamespaced field_187505_a = new net.minecraft.util.registry.RegistryNamespaced();
    public final ResourceLocation soundName;

    public SoundEvent(ResourceLocation p_i46834_1_) {
        this.soundName = p_i46834_1_;
    }

    public static void registerSound(String p_registerSound_0_) {
        ResourceLocation resourcelocation = new ResourceLocation(p_registerSound_0_);
        Minecraft.getMinecraft().getSoundHandler().sndRegistry.registerSound(new SoundEventAccessorComposite(resourcelocation,1.0d,1.0d, SoundCategory.MASTER));
    }


}
