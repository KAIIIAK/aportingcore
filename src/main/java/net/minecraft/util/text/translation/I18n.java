package net.minecraft.util.text.translation;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

//import net.minecraft.client.resources.I18n;
@SideOnly(Side.CLIENT)
public class I18n {
    public static String translateToLocal(String p_format_0_, Object... p_format_1_){
       return net.minecraft.client.resources.I18n.format(p_format_0_,p_format_1_);
    }
}
