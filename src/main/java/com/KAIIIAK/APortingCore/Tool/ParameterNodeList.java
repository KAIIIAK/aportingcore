package com.KAIIIAK.APortingCore.Tool;

import org.objectweb.asm.tree.*;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ParameterNodeList {

    public static List<ParameterNode> GetNewOne(){
        return  new List<ParameterNode>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<ParameterNode> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(ParameterNode parameterNode) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends ParameterNode> c) {
                return false;
            }

            @Override
            public boolean addAll(int index, Collection<? extends ParameterNode> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }

            @Override
            public ParameterNode get(int index) {
                return null;
            }

            @Override
            public ParameterNode set(int index, ParameterNode element) {
                return null;
            }

            @Override
            public void add(int index, ParameterNode element) {

            }

            @Override
            public ParameterNode remove(int index) {
                return null;
            }

            @Override
            public int indexOf(Object o) {
                return 0;
            }

            @Override
            public int lastIndexOf(Object o) {
                return 0;
            }

            @Override
            public ListIterator<ParameterNode> listIterator() {
                return null;
            }

            @Override
            public ListIterator<ParameterNode> listIterator(int index) {
                return null;
            }

            @Override
            public List<ParameterNode> subList(int fromIndex, int toIndex) {
                return null;
            }
        };
    }
}
