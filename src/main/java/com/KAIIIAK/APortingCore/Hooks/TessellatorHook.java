package com.KAIIIAK.APortingCore.Hooks;

import alexsocol.asjlib.asm.HookField;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.versioning.ArtifactVersion;
import cpw.mods.fml.common.versioning.VersionRange;
import gloomyfolken.hooklib.asm.Hook;
import gloomyfolken.hooklib.asm.ReturnCondition;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.WorldVertexBufferUploader;


import static net.minecraft.client.renderer.Tessellator.instance;

public class TessellatorHook {
    @HookField(targetClassName = "net.minecraft.client.renderer.Tessellator")
    public  VertexBuffer worldRenderer;
    @HookField(targetClassName = "net.minecraft.client.renderer.Tessellator")
    public WorldVertexBufferUploader vboUploader;

    @Hook(targetMethod = "<init>")
    public static void Tessellator(Tessellator ts, int p_i1250_1_){
        ts.worldRenderer = new VertexBuffer(p_i1250_1_);
        ts.vboUploader = new WorldVertexBufferUploader();
    }

    @Hook(targetMethod = "<init>")
    public static void Tessellator(Tessellator ts){
        ts.vboUploader = new WorldVertexBufferUploader();
    }
    /*@Hook(returnCondition = ReturnCondition.ALWAYS,createMethod = true)
    public static Tessellator getInstance(Tessellator ts){
        return instance;
    }*/

    @Hook()
    public static void draw(Tessellator ts){
        ts.worldRenderer.finishDrawing();
        ts.vboUploader.draw(ts.worldRenderer);
    }



}
