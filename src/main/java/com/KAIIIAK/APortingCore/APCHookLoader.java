package com.KAIIIAK.APortingCore;

import alexsocol.asjlib.asm.ASJASM;
import com.KAIIIAK.APortingCore.asm.*;
import cpw.mods.fml.common.FMLModContainer;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.ModContainerFactory;
import gloomyfolken.hooklib.asm.AsmHook;
import gloomyfolken.hooklib.asm.ReturnCondition;
import gloomyfolken.hooklib.minecraft.HookLoader;
import org.objectweb.asm.AnnotationVisitor;
import gloomyfolken.hooklib.minecraft.PrimaryClassTransformer;
import org.objectweb.asm.Type;

import java.lang.annotation.Annotation;
import java.util.HashMap;

public class APCHookLoader extends HookLoader {
    //public static boolean
    @Override
    public String[] getASMTransformerClass() {
        return new String[] {PrimaryClassTransformer.class.getName(), ASJASM.class.getName(), ASMAnnotation.class.getName(), ASMModAnnotation.class.getName(), ASMMetodParametrs.class.getName(), TesInstTransf.class.getName(),ASMMetodInstructionsCustom.class.getName()};//,ASMExtends.class.getName()};
    }

    @Override
    public void registerHooks() {
        ASJASM.registerFieldHookContainer("com.KAIIIAK.APortingCore.Hooks.TessellatorHook");
        ASJASM.registerFieldHookContainer("com.KAIIIAK.APortingCore.Hooks.ClientChatReceivedEventHook");


        ModContainerFactory.instance().registerContainerType(Type.getType(net.minecraftforge.fml.common.Mod.class), FMLModContainer.class);

        registerHookContainer("com.KAIIIAK.APortingCore.Hooks.ModMiecraftVersionChekHook");
        registerHookContainer("com.KAIIIAK.APortingCore.Hooks.TessellatorHook");
        registerHookContainer("com.KAIIIAK.APortingCore.Hooks.PropertyHook");
        registerHookContainer("com.KAIIIAK.APortingCore.Hooks.GuiOpenEventHook");
       // registerHookContainer("com.KAIIIAK.APortingCore.Hooks.RegistryNamespacedHook");
        registerHookContainer("com.KAIIIAK.APortingCore.Hooks.PositionedSoundRecordHook");




    }
}
